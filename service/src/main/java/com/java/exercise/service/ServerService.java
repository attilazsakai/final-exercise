package com.java.exercise.service;

import com.java.exercise.model.Server;
import com.java.exercise.repository.ServerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ServerService {

    private ServerRepository serverRepository;

    @Autowired
    public ServerService(ServerRepository serverRepository) {
        this.serverRepository = serverRepository;
    }

    public Server createServer(Server server) {
        return serverRepository.save(server);
    }

    public void updateServer(Server server) {
        serverRepository.save(server);
    }

    public void deleteServer(Integer id) {
        Optional<Server> server = serverRepository.findById(id);
        if(server.isPresent()) {
            serverRepository.delete(server.get());
        }
    }

    public Server getServer(int id) {
        return serverRepository.findById(id).orElse(null);
    }

    public List<Server> getAllServer() {
        return (List<Server>)serverRepository.findAll();
    }

    public Server getServerByLogicalName(String logicalName) {
        return serverRepository.findServerByLogicalName(logicalName);
    }
}
