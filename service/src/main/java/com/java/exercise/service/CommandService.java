package com.java.exercise.service;

import com.java.exercise.model.Command;
import com.java.exercise.repository.CommandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommandService {

    private CommandRepository commandRepository;

    @Autowired
    public CommandService(CommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command createCommand(Command command) {
        return commandRepository.save(command);
    }

    public List<Command> getCommands() {
        return (List<Command>) commandRepository.findAll();
    }

    public List<Command> getCommandsByUser(int userId) {
        return (List<Command>) commandRepository.findCommandsByUser(userId);
    }

}
