package com.java.exercise.service;

import com.java.exercise.model.Cluster;
import com.java.exercise.model.Server;
import com.java.exercise.repository.ClusterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClusterService {

    private ClusterRepository clusterRepository;

    @Autowired
    public ClusterService(ClusterRepository clusterRepository) {
        this.clusterRepository = clusterRepository;
    }

    public Cluster createCluster(Cluster cluster) {
        return clusterRepository.save(cluster);
    }

    public void updateCluster(Cluster cluster) {
        clusterRepository.save(cluster);
    }

    public void deleteCluster(int id) {
        Optional<Cluster> cluster = clusterRepository.findById(id);
        if(cluster.isPresent()) {
            clusterRepository.delete(cluster.get());
        }
    }

    public Cluster getCluster(int id) {
        return clusterRepository.findById(id).orElse(null);
    }

    public Cluster getClusterByName(String name) {
        return clusterRepository.findByName(name);
    }

    public List<Cluster> getAllCluster() {
        return (List<Cluster>) clusterRepository.findAll();
    }

    public List<Cluster> getByUserId(int id) {
        return clusterRepository.findByUser_Id(id);
    }

    public Cluster getByServer(Server server) {
        return clusterRepository.findByServer(server);
    }

}
