package com.java.exercise.service;

import com.java.exercise.model.State;
import com.java.exercise.repository.StateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StateService {

    private StateRepository stateRepository;

    @Autowired
    public void StateRepository(StateRepository stateRepository) {
        this.stateRepository = stateRepository;
    }

    public State createState(State state) {
        return stateRepository.save(state);
    }

    public List<State> getAllByServerId(int serverId) {
        return stateRepository.findAllByServer_Id(serverId);
    }

}
