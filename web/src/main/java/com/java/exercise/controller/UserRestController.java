package com.java.exercise.controller;

import com.java.exercise.model.Cluster;
import com.java.exercise.model.User;
import com.java.exercise.service.ClusterService;
import com.java.exercise.service.CommandService;
import com.java.exercise.service.ServerService;
import com.java.exercise.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserRestController {

    @Autowired
    private UserService userService;

    @Autowired
    private ClusterService clusterService;

    @Autowired
    private ServerService serverService;

    @Autowired
    private CommandService commandService;

    @PostMapping(path = "/add")
    public ResponseEntity<User> postUser(@RequestBody User user) {
        return new ResponseEntity<User>(userService.createUser(user), HttpStatus.CREATED);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<User> getUser(@PathVariable("id") int id) {
        User user = userService.getUser(id);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(user, HttpStatus.OK);
        }
    }

    @GetMapping("/user-list")
    public ResponseEntity<List<User>> getUserList() {
        List<User> users = userService.getAllUser();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<User> deleteUser(@PathVariable("id") int id) {
    userService.deleteUser(id);
    return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(path = "/update/{id}")
    public ResponseEntity<User> putUser(@PathVariable("id") int id, @RequestBody User user) {
        user.setId(id);
        userService.updateUser(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = "/deactivate/{id}")
    public ResponseEntity<User> deactivateUser(@PathVariable("id") int id, @RequestBody User user) {
        user.setId(id);
        user.setActivated(false);
        userService.updateUser(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = "/activate/{id}")
    public ResponseEntity<User> activateUser(@PathVariable("id") int id, @RequestBody User user) {
        user.setId(id);
        user.setActivated(true);
        userService.updateUser(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/get")
    public ResponseEntity<User> getUserByName(@RequestParam String name) {
        return new ResponseEntity<>(userService.getUserByName(name), HttpStatus.OK);
    }

    @GetMapping("/get-clusters/{id}")
    public ResponseEntity<List<Cluster>> getClusterByUserId(@PathVariable("id") int id) {
        return new ResponseEntity<>(clusterService.getByUserId(id), HttpStatus.OK);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
