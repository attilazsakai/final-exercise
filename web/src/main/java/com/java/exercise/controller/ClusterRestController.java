package com.java.exercise.controller;

import com.java.exercise.model.Cluster;
import com.java.exercise.model.Server;
import com.java.exercise.service.ClusterService;
import com.java.exercise.service.ServerService;
import com.java.exercise.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cluster")
public class ClusterRestController {

    @Autowired
    private ClusterService clusterService;

    @Autowired
    private UserService userService;

    @Autowired
    private ServerService serverService;

    @PostMapping(path = "/add")
    public ResponseEntity<Cluster> postCluster(@RequestBody Cluster cluster) {
        return new ResponseEntity<>(clusterService.createCluster(cluster), HttpStatus.CREATED);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Cluster> getCluster(@PathVariable("id") int id) {
        Cluster cluster = clusterService.getCluster(id);
        if(cluster == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(cluster, HttpStatus.OK);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Cluster> deleteCluster(@PathVariable("id") int id) {
        clusterService.deleteCluster(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(path = "/update/{id}")
    public ResponseEntity<Cluster> putCluster(@PathVariable("id") int id, Cluster cluster) {
        cluster.setId(id);
        clusterService.updateCluster(cluster);
        return new ResponseEntity<>(cluster, HttpStatus.OK);
    }

    @GetMapping(path = "/assign-user")
    public ResponseEntity<Cluster> assignUserToCluster(@RequestParam int clusterId, @RequestParam int userId) {
        Cluster cluster = clusterService.getCluster(clusterId);
        cluster.getUser().add(userService.getUser(userId));
        clusterService.updateCluster(cluster);
        return new ResponseEntity<>(cluster, HttpStatus.OK);
    }

    @GetMapping(path = "/remove-user")
    public ResponseEntity<Cluster> removeUserFromCluster(@RequestParam int clusterId, @RequestParam int userId) {
        Cluster cluster = clusterService.getCluster(clusterId);
        cluster.getUser().remove(userService.getUser(userId));
        clusterService.updateCluster(cluster);
        return new ResponseEntity<>(cluster, HttpStatus.OK);
    }

    @GetMapping(path = "/add-server")
    public ResponseEntity<Cluster> addServerToCluster(@RequestParam int clusterId, @RequestParam int serverId) {
        Cluster cluster = clusterService.getCluster(clusterId);
        cluster.getServer().add(serverService.getServer(serverId));
        clusterService.updateCluster(cluster);
        return new ResponseEntity<>(cluster, HttpStatus.OK);
    }

    @GetMapping(path = "/remove-server")
    public ResponseEntity<Cluster> removeServerToCluster(@RequestParam int clusterId, @RequestParam int serverId) {
        Cluster cluster = clusterService.getCluster(clusterId);
        cluster.getServer().remove(serverService.getServer(serverId));
        clusterService.updateCluster(cluster);
        return new ResponseEntity<>(cluster, HttpStatus.OK);
    }

    @GetMapping(path = "/get-servers-in-cluster/{id}")
    public ResponseEntity<List<Server>> getServersInCluster(@PathVariable("id") int clusterId) {
        return new ResponseEntity<>((List<Server>) clusterService.getCluster(clusterId).getServer(), HttpStatus.OK);
    }

    @GetMapping(path = "/get-cluster-by-name/{name}")
    public ResponseEntity<Cluster> getClusterByName(@PathVariable("name") String name) {
        return new ResponseEntity<>(clusterService.getClusterByName(name), HttpStatus.OK);
    }

    @GetMapping(path = "/list-all")
    public ResponseEntity<List<Cluster>> getAllCluster() {
        return new ResponseEntity<>(clusterService.getAllCluster(), HttpStatus.OK);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
