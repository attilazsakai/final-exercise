package com.java.exercise.controller;

import com.java.exercise.model.Server;
import com.java.exercise.model.State;
import com.java.exercise.service.ServerService;
import com.java.exercise.service.StateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/server")
public class ServerRestController {

    @Autowired
    private ServerService serverService;

    @Autowired
    private StateService stateService;

    private static final Pattern IP_ADDRESS_PATTERN = Pattern.compile(
            "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");


    @PostMapping(path = "/add")
    public ResponseEntity<Object> postUser(@RequestBody Server server) {
        if (IP_ADDRESS_PATTERN.matcher(server.getIpAddress()).matches()) {
            return new ResponseEntity<>(serverService.createServer(server), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("Please give a valid ip address!", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Server> getServer(@PathVariable("id") int id) {
        Server server = serverService.getServer(id);
        if(server == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(server, HttpStatus.OK);
        }
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Server> deleteServer(@PathVariable("id") int id) {
        serverService.deleteServer(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(path = "/update/{id}")
    public ResponseEntity<Object> putServer(@PathVariable("id") int id, @RequestBody Server server) {

        if (IP_ADDRESS_PATTERN.matcher(server.getIpAddress()).matches()) {
            server.setId(id);
            serverService.updateServer(server);
            return new ResponseEntity<>(server, HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Please give a valid ip address!", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/get-server-by-name/{name}")
    public ResponseEntity<Server> getServerByLogicalName(@PathVariable("name") String name) {
        return new ResponseEntity<>(serverService.getServerByLogicalName(name), HttpStatus.OK);
    }

    @GetMapping("/server-list")
    public ResponseEntity<List<Server>> getServerByLogicalName() {
        return new ResponseEntity<>(serverService.getAllServer(), HttpStatus.OK);
    }

    @GetMapping("/get-previous-states/{id}")
    public ResponseEntity<List<State>> getPreviousStates(@PathVariable("id") int serverId) {
        return new ResponseEntity<>(stateService.getAllByServerId(serverId), HttpStatus.OK);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(Exception e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
