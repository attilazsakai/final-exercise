package com.java.exercise.controller;

import com.java.exercise.model.Cluster;
import com.java.exercise.model.Command;
import com.java.exercise.model.Server;
import com.java.exercise.model.State;
import com.java.exercise.model.StateType;
import com.java.exercise.model.User;
import com.java.exercise.service.ClusterService;
import com.java.exercise.service.CommandService;
import com.java.exercise.service.ServerService;
import com.java.exercise.service.StateService;
import com.java.exercise.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/command")
public class CommandRestController {

    @Autowired
    private CommandService commandService;

    @Autowired
    private UserService userService;

    @Autowired
    private ClusterService clusterService;

    @Autowired
    private ServerService serverService;

    @Autowired
    private StateService stateService;


    @GetMapping("/run")
    public ResponseEntity<Object> runCommand(@RequestBody Command command, @RequestParam int userId, @RequestParam int serverId) {
        if (command.getType() != null) {
            switch(command.getType()) {
                case "SHUTDOWN" : return shutdown(command, userId, serverId);
                case "STARTUP": return startup(command, userId, serverId);
                case "RUNPROCESS": return runProcess(command, userId, serverId);
                case "STOPPROCESS": return stopProcess(command, userId, serverId);
                default: return new ResponseEntity<>("The command: " + command.getType() + " does not exist!", HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>("The command cannot be null!", HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/command-list")
    public ResponseEntity<List<Command>> getCommandList() {
        return new ResponseEntity<>(commandService.getCommands(), HttpStatus.OK);
    }

    @GetMapping("/state-list/{id}")
    public ResponseEntity<List<State>> getStateList(@PathVariable("id") int serverId) {
        return new ResponseEntity<>(stateService.getAllByServerId(serverId), HttpStatus.OK);
    }

    @GetMapping("/commands-by-user-id")
    public ResponseEntity<List<Command>> getCommandsByUserId(@RequestParam int userId) {
        return new ResponseEntity<>(commandService.getCommandsByUser(userId), HttpStatus.OK);
    }

    private ResponseEntity<Object> shutdown(final Command command, final int userId, final int serverId) {
        final User user = userService.getUser(userId);
        if (user.getActivated()) {
            final Server server = serverService.getServer(serverId);
            final Cluster cluster = clusterService.getByServer(server);
            if(cluster.getUser().contains(user) && server.getActualState().equals(StateType.WAITING)) {
                server.setActualState(StateType.SHUTDOWN);
                return buildStateAndCommand(command, user, server, StateType.SHUTDOWN);
            } else {
                return new ResponseEntity<>("The server with the following id: " + serverId +
                        " is not assigned to the user or it is currently not available!", HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>("The user with the following id: "
                    + userId + " is inactive!", HttpStatus.BAD_REQUEST);
        }
    }

    private ResponseEntity<Object> startup(final Command command, final int userId, final int serverId) {
        final User user = userService.getUser(userId);
        if (user.getActivated()) {
            final Server server = serverService.getServer(serverId);
            final Cluster cluster = clusterService.getByServer(server);
            if(cluster.getUser().contains(user) && server.getActualState().equals(StateType.SHUTDOWN)) {
                server.setActualState(StateType.WAITING);
                return buildStateAndCommand(command, user, server, StateType.WAITING);
            } else {
                return new ResponseEntity<>("The server with the following id: " + serverId +
                        " is not assigned to the user or it is currently not available!", HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>("The user with the following id: "
                    + userId + " is inactive!", HttpStatus.BAD_REQUEST);
        }
    }

    private ResponseEntity<Object> runProcess(final Command command, final int userId, final int serverId) {
        final User user = userService.getUser(userId);
        if (user.getActivated()) {
            final Server server = serverService.getServer(serverId);
            final Cluster cluster = clusterService.getByServer(server);
            if(cluster.getUser().contains(user) && server.getActualState().equals(StateType.WAITING)) {
                server.setActualState(StateType.RUNNING);
                return buildStateAndCommand(command, user, server, StateType.RUNNING);
            } else {
                return new ResponseEntity<>("The server with the following id: " + serverId +
                        " is not assigned to the user or it is currently not available!", HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>("The user with the following id: "
                    + userId + " is inactive!", HttpStatus.BAD_REQUEST);
        }
    }

    private ResponseEntity<Object> stopProcess(final Command command, final int userId, final int serverId) {
        final User user = userService.getUser(userId);
        if (user.getActivated()) {
            final Server server = serverService.getServer(serverId);
            final Cluster cluster = clusterService.getByServer(server);
            if(cluster.getUser().contains(user) && server.getActualState().equals(StateType.RUNNING)) {
                server.setActualState(StateType.WAITING);
                return buildStateAndCommand(command, user, server, StateType.WAITING);
            } else {
                return new ResponseEntity<>("The server with the following id: " + serverId +
                        " is not assigned to the user or it is currently not available!", HttpStatus.BAD_REQUEST);
            }
        } else {
            return new ResponseEntity<>("The user with the following id: "
                    + userId + " is inactive!", HttpStatus.BAD_REQUEST);
        }
    }

    private ResponseEntity<Object> buildStateAndCommand(Command command, User user, Server server, StateType type) {
        command.setServer(server);
        command.setUser(user);
        State state = new State();
        state.setProcess(command.getProcess());
        state.setServer(server);
        state.setTimestamp(new Timestamp(System.currentTimeMillis()));
        state.setState(type);
        stateService.createState(state);
        return new ResponseEntity<>(commandService.createCommand(command), HttpStatus.OK);
    }
}
