package com.java.exercise.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import java.util.Collection;

@Getter
@Setter
@Entity
public class Cluster {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(unique=true)
    private String name;
    @ManyToMany
    @JoinColumn(name = "user_id")
    private Collection<User> user;
    @OneToMany
    @JoinColumn(name = "server_id")
    private Collection<Server> server;

}
