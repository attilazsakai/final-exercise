package com.java.exercise.model;

public enum StateType {
    SHUTDOWN,
    WAITING,
    RUNNING,
}
