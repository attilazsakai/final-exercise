package com.java.exercise.repository;

import com.java.exercise.model.State;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StateRepository extends CrudRepository<State, Integer> {

    List<State> findAllByServer_Id (int serverId);
}
