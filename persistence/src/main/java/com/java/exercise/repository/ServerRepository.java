package com.java.exercise.repository;

import com.java.exercise.model.Server;
import org.springframework.data.repository.CrudRepository;

public interface ServerRepository extends CrudRepository<Server, Integer> {

    Server findServerByLogicalName(String logicalName);
}
