package com.java.exercise.repository;

import com.java.exercise.model.Cluster;
import com.java.exercise.model.Server;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClusterRepository extends CrudRepository<Cluster, Integer> {

    List<Cluster> findByUser_Id(int id);

    Cluster findByName(String name);

    Cluster findByServer(Server server);
}
