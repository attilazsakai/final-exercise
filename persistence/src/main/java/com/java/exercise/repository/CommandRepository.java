package com.java.exercise.repository;

import com.java.exercise.model.Command;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CommandRepository extends CrudRepository<Command, Integer> {

    List<Command> findCommandsByUser(int userId);
}
